# liri-node-app
A command line based script that utilizes Twitter, Spotify, and OMDB's API's.

Users are able to read their last 20 tweets (from oldest to newest, as was the goal of this project), as well as request song or movie information.
You will need to provide environmental variables for your Twitter, Spotify, and OMDB API keys to utilize this program.